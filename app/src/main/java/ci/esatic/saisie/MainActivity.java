package ci.esatic.saisie;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText txtNom, txtPnom;
    Button btnValider, btnAnnuler;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNom = (EditText) findViewById(R.id.text_nom);
        txtPnom = (EditText) findViewById(R.id.text_pnom);
        btnAnnuler = (Button) findViewById(R.id.btn_annuler);
        btnValider = (Button) findViewById(R.id.btn_valider);

        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNom.setText("");
                txtPnom.setText("");
            }
        });

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNom.getText().toString().trim().equals("") || txtPnom.getText().toString().trim().equals("")){
                    dialog = new AlertDialog.Builder(MainActivity.this) // Pass a reference to your main activity here
                            .setTitle("Message")
                            .setMessage("Veuillez remplir tous les champs SVp !")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i)
                                {
                                    dialog.cancel();
                                }
                            })
                            .show();
                } else {
                    dialog = new AlertDialog.Builder(MainActivity.this) // Pass a reference to your main activity here
                            .setTitle("Message")
                            .setMessage("Je suis " + txtNom.getText().toString() + " " + txtPnom.getText().toString())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i)
                                {
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            }
        });
    }
}